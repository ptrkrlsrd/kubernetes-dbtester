package main

import (
	"database/sql"
	"fmt"
	"log"
	"os"

	_ "github.com/lib/pq"
)

func main() {
	connectionString := os.Getenv("POSTGRES_CONNECTION_STRING")
	host := os.Getenv("POSTGRES_SERVICE_SERVICE_HOST")
	log.SetFlags(0)
	
	db, err := sql.Open("postgres", connectionString)
	if err != nil {
		log.Fatal(err)
	}

	rows, err := db.Query("SELECT schema_name FROM information_schema.schemata;")
	if err != nil {
		log.Fatal(err)
	}

	for rows.Next() {
		var schema string
		if err := rows.Scan(&schema); err != nil {
			log.Fatalf("failed scanning row: %v", err)
			os.Exit(1)
		}

		fmt.Println(schema)
	}
	
	os.Exit(0)
}
