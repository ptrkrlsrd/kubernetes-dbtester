FROM golang:latest as build-stage
WORKDIR /dbtester
COPY go.mod .
COPY main.go .
RUN go get -v
RUN CGO_ENABLED=0 GOOS=linux go build -a -installsuffix cgo -o main .

FROM alpine:latest
RUN apk add ca-certificates
ENV GIN_MODE "release"
COPY --from=build-stage /dbtester/main /
ENTRYPOINT ["/main"]
